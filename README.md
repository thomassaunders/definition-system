# Definition System
> A system for creating simple dictionaries.

Often with interests and industries there are various words and acronyms that are not self explanatory.  This program is designed to make it easy to give information about and words or acronyms specific to your interest or industry to others for them to understand.

## Installation

Universal:
Run the .jar file and enjoy!

## Development setup

TODO:  Development setup instructions

## Release History

No releases thus far.

## Meta

Thomas Saunders – [@thomass379](https://twitter.com/thomass379) – thomassaunders379@gmail.com

Distributed under the MIT license. See ``LICENSE`` for more information.

[https://github.com/thomassaunders](https://github.com/thomassaunders/)

## Contributing

1. Fork it (<https://github.com/thomassaunders/Definition-System/fork>)
1.1 Use the Develop branch for the latest code or master for stable
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request
